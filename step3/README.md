# RES HTTP Infra lab step 3

This step contains dockerfiles for multiple containers:

- Static server based on step 1.
- Dynamic server based on step 2.
- Reverse proxy connecting to the aforementioned containers.

Please see the READMEs in the corresponding steps for info;
The rest of the document assumes you have read such documents and proceeds by
explaining only the differences.

## How to use

### Build the container images

```console
docker build . -f appserver.dockerfile -t res-step3-appserver-img
docker build . -f webserver.dockerfile -t res-step3-webserver-img
docker build . -f reverse_proxy.dockerfile -t res-step3-reverse_proxy-img
```

The first command asks docker to build the docker image `appserver.dockerfile`,
with the build context set to `.` (the current directory); the image is then
tagged with the name `res-step3-appserver-img`.
The other commands have similar syntax.

### How to run

In this setup we take advantage of docker's service discovery, by using a
[bridged network][bridged], which provides automatic [DNS][dns] resolution
between containers.

[bridged]: https://docs.docker.com/network/bridge/
[dns]: https://en.wikipedia.org/wiki/Domain_Name_System

> It is worth noting we could also use an overlay network (provided by docker
> swarm), which would allow us to run traffic across container hosts, but the
> setup and explanations are outside of the scope of the lab, the reader is
> nonetheless advised to try such configuration.

We start by creating a new network.
A network is a connectivity namespace where containers may communicate.
The advantage of using a network here is that it allows us to use the container
name as host, which will be resolved by the DNS server provided by the docker
daemon to resolve the IP address of the respective container.

```console
docker network create -d bridge --attachable res-step3
```

```console
docker run --network res-step3 --name res-step3-webserver -d res-step3-webserver-img
docker run --network res-step3 --name res-step3-appserver -d res-step3-appserver-img
docker run --network res-step3 --name res-step3-reverse_proxy -p 27003:80 res-step3-reverse_proxy-img
```

The first command will start by creating a new docker container from the
`res-step3-webserver-img`. After the docker daemon has created the container it
will be immediately started and the command will return.
The newly created container will be given name `res-step3-webserver`.
The second command performs something very similar, but for the appserver
instead.

The third command is a bit more special.
Note the previous two commands contained no port bindings as in the previous
steps (which will be enlightened in later section).
This command binds port 27003 on the host to port 80 in the container.

## Dockerfile, explained

`webserver.dockerfile` and `appserver.dockerfile` were explained in steps 1 and
2 respectively, please refer to those READMEs for details.

`reverse_proxy.dockerfile` uses the `nginx` image as base, removes the default
(example) site from the nginx configuration and installs (copy) the
`reverse_proxy.conf` configuration file (detailed in a later section).
Note that this will inherit the `ENTRYPOINT` from the `nginx` image so that
when the container is ran, it will execute the nginx server.

## Reverse proxy, explained

This image features a [reverse proxy][reverse_proxy] configuration serving two
different domains (`localhost` and `appserver.localhost`).
Various 'standard' headers are attached to the forwarded request for good
practice reasons, but they are (for the moment) not used by either application.

Requests directed towards the first domain will be routed to the webserver and
those directed towards the later to the application server.

[reverse_proxy]: https://en.wikipedia.org/wiki/Reverse_proxy

The reverse proxy container takes advantage of docker's service discovery
feature in [bridged networks][bridged].
Docker networks provide an isolated networking namespace where containers in the
same namespace can communicate between themselves without restrictions and they
can dynamically resolve IP addresses from container names, this allows an extra
level of resilience with respect to the solution proposed in the lectures where
the reverse proxy configuration would target IPs.

Since we do not need to directly contact either of the web and
application servers, we only expose port 80 in the reverse proxy.
Given that no ports in the other containers were exposed to the host, the only
way of reaching them is through the reverse proxy.

The application server may be accessed at <http://appserver.localhost:27003/>,
the web server may be accessed at <http://localhost:27003/>.

> Note that RFC 6761, section 6.3 (Domain Name Reservation Considerations for
> "localhost.") regulates the 'localhost' domain and '*.localhost' space:
>
> > The domain "localhost." and any names falling within ".localhost."
> > are special in the following ways:
> (...) Section 3:
> > Name resolution APIs and libraries SHOULD recognize localhost
> > names as special and SHOULD always return the IP loopback address
> > for address queries and negative responses for all other query
> > types.  Name resolution APIs SHOULD NOT send queries for
> > localhost names to their configured caching DNS server(s).
>
> Thus the use of any localhost subdomain should be portable across platforms.

### Limitations

As described in the section above, the presented solution does not have the same
limitations as the one showcased on the lectures.
We do however identify the following weaknesses:

- The names under which the containers must be launched are fixed.
  This implies we cannot have multiple instances of the same service, regardless
  of the docker network they are in.

- The names of the target containers are hardcoded in the nginx site
  configuration.
  A more portable solution (such as those provided by Kubernetes) would pin the
  'type' of service instead of the actual implementation.
