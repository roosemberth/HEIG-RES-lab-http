# vim:ft=dockerfile
FROM nginx

# Remove default site, prevents mishaps
RUN rm /etc/nginx/conf.d/default.conf
COPY reverse_proxy.conf /etc/nginx/conf.d
