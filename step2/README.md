# RES HTTP Infra lab step 2

This container runs a webserver serving a WSGI application.
The webserver used is NGINX. The application server is hosted by [uWSGI][uwsgi],
a modern application container server. The application server is written in
python3 using [flask][flask], a micro web framework for python applications.

[uwsgi]: https://uwsgi-docs.readthedocs.io/en/latest/
[flask]: https://flask.palletsprojects.com/en/2.0.x/

The current configuration of uwsgi automatically scales between 2 and 16
threads, on demand by the webserver.

The application server is written in python3 and can be found inside the `app`
module (directory).

## How to use

The container must be built before it can be ran and operated.
See instructions below.

### Build the image

```console
docker build . -t res-step2-img
```

### Run the image

```console
docker run --name res-step2 -p 27001:80 res-step2-img
```

## Dockerfile, explained

The dockerfile here is also trivial enough, it merely pulls the image
`tiangolo/uwsgi-nginx-flask:python3.8` published on Docker Hub.
The image is configured to run uWSGI and Nginx respectively by loading the
`app` module.

## Application server, explained

The application server is also fairly trivial, it serves a static page with a
link to a json endpoint which will return dynamically-generated json content.

An example response (using httpie) looks like this:

```console
$ http :27001/json
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 55
Content-Type: application/json
Date: Fri, 28 May 2021 22:00:57 GMT
Server: nginx/1.17.10

{
    "random-uuid": "5cf2b259-3318-4dbd-9e83-25c3eefcfe0c"
}
```
