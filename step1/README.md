# RES HTTP Infra lab step 1

This runs an NGINX-based container serving a static website on `/`.
(whatever is in the `static-content/` folder.)

## How to use

The container must be built before it can be ran and operated.
See instructions below.

### Build the image

```console
docker build . -t res-step1-img
```

This command asks docker to build a docker image according to the `Dockerfile`
file found in the `.` (current) directory; after building, tag it with the name
`res-step1-img` for easy access.

### Run the image

```console
docker run --name res-step1 -p 27000:80 res-step1-img
```

This command will start by creating a new docker container from the
`res-step1-img`, configuring to bind to host port 27000 and forward it to port
80 when the container is started. After the docker daemon has created the
container it will be immediately started. The newly created container will be
given name `res-step1`.

Depending on your terminal emulator, you may use the `Ctrl-c` or `Ctrl-\` keys to kill the container.
Depending on your terminal emulator, you may detach from the running container without killing it by pressing `Ctrl-p` then `Ctrl-q`.

### Attach to a running container

```console
docker attach res-step1
```

This command can be used to attach to the running container with name
`res-step1`.

### Start the container after it was stopped

If the container was previously created, but then stopped (either by a keyboard
shortcut such as `Ctrl-c` or because your computer rebooted) it can be restarted
with the following command:

```console
docker start $(RUNNAME)
```

You can list all existing containers (running or not) with `docker ps -a`.

## Dockerfile, explained

The dockerfile here is fairly trivial, it merely pulls the image `nginx`
published on [Docker Hub][dockerhub], which is the community repository of OCI
and docker container images.

[dockerhub]: https://hub.docker.com/

After pulling the image, we copy the folder `static-content/` into
`/usr/share/nginx/html` inside the container, which with the pre-populated nginx
configuration in the container will server these files under the root of the
webserver.

## Static content

The files in the `./static-content` directory were downloaded from
<https://startbootstrap.com/template/bare> and slightly modified show the name
of the project members.
