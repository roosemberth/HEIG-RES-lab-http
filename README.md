# HTTP infra lab

This repository contains the many phases of the [HTTP infra laboratory][lab] for
the [RES][res] course at [HEIG-VD][heig].

[lab]: https://github.com/SoftEng-HEIGVD/Teaching-HEIGVD-RES-2021-Labo-HTTPInfra
[res]: https://github.com/SoftEng-HEIGVD/Teaching-HEIGVD-RES-2021
[heig]: https://heig-vd.ch/

This project was made (exceptionally) by a group of three students:

- Roosembert Palacios
- Florian Gazzetta
- Joan Maillard

Each step gradually builds on the previous ones, but if you're in a rush, you
can directly read `all-steps-with-bonus/`.
Although each step builds on top of previous ones we decided to keep them in
separate directories since it allows complete freedom to replace previous
components without overseeing the replaced changes.
