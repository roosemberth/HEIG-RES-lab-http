import uuid

from flask import Flask, jsonify

app = Flask(__name__)

filename = "page.html"

@app.route("/api/")
def index():
  with open('page.html', 'r') as file:
    page = file.read().replace('\n', '')
  return page

@app.route("/api/json")
def json():
  data = {"random-uuid": uuid.uuid4()};
  return jsonify(data)

# Used only for development
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
