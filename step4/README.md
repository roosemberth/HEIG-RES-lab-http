# RES HTTP Infra lab step 4

This steps builds upon the foundations provided by step 3.
Please read step 3's README if you have not done so yet.

## How to use

### Build the container images

```console
docker build . -f appserver.dockerfile -t res-step4-appserver-img
docker build . -f webserver.dockerfile -t res-step4-webserver-img
docker build . -f reverse_proxy.dockerfile -t res-step4-reverse_proxy-img
```

The commands are very similar to those in the previous step, please refer the
previous step's README for details.

### How to run

Yet again, we take advantage of docker's service discovery feature.

We start by creating a new network.
This will allow containers to address themselves by name.

```console
docker network create -d bridge --attachable res-step4
```

Yet again, we launch the necessary containers.

```console
docker run --network res-step4 --name res-step4-webserver -d res-step4-webserver-img
docker run --network res-step4 --name res-step4-appserver -d res-step4-appserver-img
docker run --network res-step4 --name res-step4-reverse_proxy -p 27004:80 res-step4-reverse_proxy-img
```

Please refer to the previous step's README for details.

## Dockerfiles and reverse proxy

The explanations in the previous step's README apply pretty much identically,
with the following differences:

- The reverse proxy listens under a single server, which is also the default
  server (hence, any requests will be responded by this server).

- The reverse proxy will send to the web server any requests under `/`, except
  those under `/api/`.
  Requests under `/api/` will be sent to the application server instead.
