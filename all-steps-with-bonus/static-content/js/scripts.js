/*!
* Start Bootstrap - Bare v5.0.0 (https://startbootstrap.com/template/bare)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-bare/blob/master/LICENSE)
*/

function feedProgressBar(id, duration, callback) {
  var progressbarinner = document.getElementById(id);
  progressbarinner.style.animation = 'none';
  progressbarinner.offsetHeight; /* Trigger reflow */
  progressbarinner.style.animation = null;
  progressbarinner.style.animationPlayState = 'running';
}


$(function() {
  function refreshUuid() {
    console.log("Refreshing UUID");
    $.getJSON("/api/json", function(apiResponse) {
      console.log(apiResponse);
      $(".token-display").text(apiResponse["random-uuid"]);
      feedProgressBar("token-timeout-progressbar");
    })
  };

  refreshUuid();
  setInterval(refreshUuid, 3000);
})
