# RES HTTP Infra lab with all steps and bonuses

This section builds on the foundations provided by the previous steps.
The reader is advised to take knowledge of the previous READMEs.

## Digression on researching a dynamic cluster management solution

> This section reflect _Roosembert_'s sole opinion.

It is worth noting that though this is one of the last points in the assignment,
it's worth considering much earlier since it may have consequences on the stack
components to use and configuring some features such as load balancing is highly
dependent on the stack used.

A recurrent scheme for load balancing is to have the reverse proxy resolve a
domain representing the service.
The DNS server can either respond with a list of backends in sorted or random
order to which the load balancer will relay traffic to.
This is a well-proven solution that I have myself set up and maintained in
production; this setup is however not responsive enough since the reverse proxy
usually only resolves the backends on start or reload, hence adding or removing
nodes requires signalling the load balancer.
Some commercial products (Such as NGINX+) provide solutions to these problems,
but they seem inherently flawed.

Another solution that I have also personally operated is to have a global
registry and configure the reverse proxy to listen to specific events.
For instance, in a Kubernetes cluster, [traefik][traefik] can listen to Ingress
resources being created in the cluster and adequately configure routers and
services to distribute load on these services.
The main advantage of this solution is that it is very well integrated with the
Kubernetes ecosystem (as a historical note, Kubernetes ingress resource were
predated by traefik's ingress controller CRDs, which are an extensible way of
experimenting with the Kubernetes ecosystem).
The main disadvantage is that it requires the many moving pieces of a Kubernetes
cluster with the bloat of many features, which depending on the objective may
be completely irrelevant to the problem at hand (such as container
orchestration, auto-scaling, rollbacks, ...).

[traefik]: https://traefik.io/

### Dynamic service registration

With these two frames of reference, I set to find a middle-ground solution that
uses a registry without managing any aspects about how the services are
provisioned, but merely consumes information from the registry.
After some research I found out that I could use [hashicorp's consul][consul]
as a service registry, have Traefik provision its routers (ie. backend groups)
and services (ie. backend instances) and have services register upon startup.

[consul]: https://www.consul.io/

After searching a bit through the internet, I found [Gliderlabs'
registrator][registrator].
_Registrator_ runs in parallel to the rest of the stack and automatically
registers and reregisters services to many service registries, such as consul
in particular

[registrator]: http://gliderlabs.github.io/registrator/latest/

The _Registrator_ works by registering docker containers as instances of a
service called _\<image-name>-\<container-port>_.
If the service does not exist, it will be created.

### Load balancer configuration

The consul ecosystem provides a side tool called `consul-template`, which
regenerates configuration files when the consul catalog changes and signals
containers after the file was updated.
This works very nicely with applications like `nginx`, which reload their
configuration on `SIGHUP`.
But since the load balancer was not a constraint, I decided to use
[traefik][traefik] since it's better integrated with registries.

## How to use

### Build the container images

```console
docker build . -f appserver.dockerfile -t res-appserver-img
docker build . -f webserver.dockerfile -t res-webserver-img
```

The commands are very similar to those in the previous step, please refer the
previous step's README for details.

### How to run

Yet again, we take advantage of docker's service discovery feature.

We start by creating a new network.
This will allow containers to address themselves by name.

```console
docker network create -d bridge --attachable res
```

In order to achieve a dynamic cluster, we use [consul][consul] to provide a
service registry.

```console
docker run --network res --name consul -p 8500:8500 --rm -d \
  -h consul-server progrium/consul -server -bootstrap
```

Naming the consul container is optional at this point.
We expose the consul REST API endpoint for debugging, but this is not needed
for normal functioning either since other services consuming this API will do
so from the same docker network, which provides unrestricted access.
We spawn a container that will be deleted when shut down.
This is common practice in distributed systems to destroy any parasitic state
and explicitly indicate stateful components.
(e.g. databases or filesystem paths).
This command also specifies the hostname of the docker container, this is merely
to have clearer logs in the consul clients.

We proceed by running [Gliderlabs' registrator][registrator].
This application will listen for events on the docker socket and register
docker containers as services to consul.

```console
docker run --network res --name consul-registrator \
  -v /var/run/docker.sock:/tmp/docker.sock --rm -d \
  -it gliderlabs/registrator -internal consul://consul:8500
```

This command will bind the docker socket into the container so that
_registrator_ will be able to bind and query the docker daemon.
The `-internal` flag is used to indicate the registrator to use the target bind
port instead of any publicly exposed one.
This also allows consul to pick any ports declared with the `EXPOSE` directive
even if they are not bound.
If the container ports were to be bound to the host, _registrator__ would prefer
the host-bound sockets instead of the ones directly attached to the container.
Using the internal flag avoids the extra routing stage where the connection
would first go to the container host network, reach the port forward and then be
redirected to the target port in the container, we can do this since the
consumers of this service are within the same network segment.

The next step is to start traefik to serve as an edge router.

If you're using a GNU/Linux operating system, you can directly run the following
command:

```console
docker run --network res --rm -p 27006:80 \
  -d -v $PWD/traefik-config.yaml:/etc/traefik/traefik.yaml traefik
```

If you're using a different operating system, chances are mounting the
configuration file won't work, in which case you'll have to build an image
with the configuration file embedded, and run that instead:

```console
docker build . -f traefik.dockerfile -t res-traefik-img
docker run --network res --rm -p 27006:80 -d res-traefik-img
```

We can finally start any number of instances of our services.

```console
docker run --network res -d --rm res-webserver-img
docker run --network res -d --rm res-webserver-img
docker run --network res -d --rm res-webserver-img
docker run --network res -d --rm res-webserver-img
docker run --network res -d --rm res-webserver-img
```

```console
docker run --network res -d --rm res-appserver-img
docker run --network res -d --rm res-appserver-img
docker run --network res -d --rm res-appserver-img
docker run --network res -d --rm res-appserver-img
docker run --network res -d --rm res-appserver-img
```

Note that we do not give names to the containers this time and that they are set
to be deleted as soon as they stop (`--rm`), it is good practice on HA clusters
to have ephemeral workers and manage any state (e.g. databases, files)
declaratively.
If our applications required any persistent file storage, they should be
explicitly provisioned to the container (e.g. using the `-v` flag).

## Consul as registry

Services are registed to consul by the _registrator_.
traefik can then query the registry using it's `consulCatalog` provider to
dynamically create services and servers (which correspond to endpoints that
are able to ultimately handle a request).

Consul is an important component in the cluster since it is required by traefik
to register and evict services and traefik servers.
In the configuration presented, it's also a [Single point of failure][spof];
this can be mitigated by launching multiple instances of consul and having them
federate.
The instances of consul must then be load-balanced, which would create a
bootstrapping problem.
Since a fallen instance of the registry won't affect end users, we decided to
leave it out for further improvements.

[spof]: https://en.wikipedia.org/wiki/Single_point_of_failure

Consul provides a web interface on port 8500, since we bound it in the docker
command, it may be directly accessed.

The _registrator_ container can, in contrast be launched multiple times since
the information created in the registry is idempotent.
No load-balancing is needed since it's a passive component.

## Traefik as an edge router

[Traefik][traefik] is an HTTP reverse proxy and load balancer.
It is cloud-aware and have first-class support for dynamic environments such as
the one described in this document.

In a typical setup, Traefik can be configured with TLS support (e.g. HTTPS, WSS,
...) and may automatically obtain SSL certificates from [Let's encrypt][le],
a Certificate Authority providing TLS certificates for _TLS Web Server
Authentication_ and _TLS Web Client Authentication_.
Mind that Let's encrypt (or traefik, for that matter) does not provide support
for non-repudiation in their TLS sessions.

[le]: https://letsencrypt.org/

Traefik has two configuration primitives: _dynamic configuration_ and
_static configuration_.
Both are used by traffic.
The static configuration MUST be supplied as a configuration file or command
line flags and the dynamic configuration can be cooperatively constructed
interactively and by other traefik components (providers) and modified at any
given time.

For brevity, we use the same file to provision both static and part of the
dynamic configuration.

In the static configuration enable the dashboard, the 'provider' plugins to be
used to complete the dynamic configuration and declare the entry points of
HTTP traffic (e.g. port 80).

In the dynamic section of the file, we declare 'routers' (which more or less
correspond to _NGINX servers_) to match rules for the traefik dashboard and our
web server and application server.

The consulCatalog provider will create a _Traefik service_ for every _consul
service_ registered.
Hence the traefik routers bind the rules controlling where traffic goes to
such services.
_Traefik services_ may contain many “servers”, which is traefik's way of
explaining that a service may be provided by one of many instances.

Finally the dashboard is mounted at <http://traefik.localhost:27006/dashboard/>,
an authentication middleware (such as basic HTTP auth or JWT token) can be
added if needed.

## Load balancing

As previously described, _Registrator_ will create _Consul services_ and _Consul
service instances_ that will be retrieved by the _Traefik consulCatalog
provider_ that will in turn create _Traefik services_ and _Traefik servers_.
_Traefik services_ represent a reachable entity that can be matched by one or
more _Traefik rules_ to route the request to _Traefik servers_.
_Traefik servers_ represent a group of resources that may be used to ultimately
handle a request.
In our case, both the _res-appserver-img_ and _res-webserver-img_ docker images,
when instantiated by at least one container, will become _Consul services_,
which will in turn become _Traefik services_.
Each running container based on the aforementioned images will be then become a
_Consul service instance_, which will in turn become a _Traefik server_.

Incoming requests will be routed by _Traefik_ to any routers active on such
endpoint (unless otherwise specified, routers are active in all endpoints).
In our case, the only endpoint is port 80.
Requests will be matched against the rules in each of the routers and dispatched
to the service in [the router with the longest match][traefik-routers].

[traefik-routers]: https://doc.traefik.io/traefik/routing/routers/#priority

Requests matched to a service will be then dispatched to any of the backing
servers of the service.
In our case, this will be dispatched to any of the instances of the image
matching the service.

### Sticky sessions

By default, when multiple servers are configured for a service, Traefik will
round-robin among the service servers.
Traefik can be configured to enable sticky sessions: When sticky sessions are
enabled, a cookie is set on the initial request and response to let the client
know which server handles the first response; on subsequent requests, to keep
the session alive with the same server the client should resend the same cookie.

The `consulCatalog` provider supports many traefik configuration features
through tags in the consul service.
The `registrator` service supports configuring many options of the registered
consul service through environment variables in the container.
Hence, in order to enable the sticky session cookie on the `res-webserver-img`
service one can simply add the
`SERVICE_TAGS=traefik.http.services.res-webserver-img.loadbalancer.sticky.cookie=true`
environment variable.

Hence the command lines for launching the webserver image can be modified as
such:

```console
docker run --network res -d -e SERVICE_TAGS=traefik.http.services.res-webserver-img.loadbalancer.sticky.cookie=true --rm res-webserver-img
docker run --network res -d -e SERVICE_TAGS=traefik.http.services.res-webserver-img.loadbalancer.sticky.cookie=true --rm res-webserver-img
docker run --network res -d -e SERVICE_TAGS=traefik.http.services.res-webserver-img.loadbalancer.sticky.cookie=true --rm res-webserver-img
docker run --network res -d -e SERVICE_TAGS=traefik.http.services.res-webserver-img.loadbalancer.sticky.cookie=true --rm res-webserver-img
docker run --network res -d -e SERVICE_TAGS=traefik.http.services.res-webserver-img.loadbalancer.sticky.cookie=true --rm res-webserver-img
```

If modifying the command line of the containers is not desirable, one can also
add the corresponding tag to the consul service manually or through its REST
API.

After configuring sticky sessions, an http request to the webserver yields the
following result:

```console
$ http --headers :27006/
HTTP/1.1 200 OK
Accept-Ranges: bytes
Content-Length: 4290
Content-Type: text/html
Date: Sat, 29 May 2021 18:16:50 GMT
Etag: "60b19305-10c2"
Last-Modified: Sat, 29 May 2021 01:04:05 GMT
Server: nginx/1.21.0
Set-Cookie: _c3239=http://172.20.0.7:80; Path=/
```

One can check only sticky sessions were enabled only in the webserver by
querying a route leading to the application server:

```console
$ http --headers :27006/api/
HTTP/1.1 200 OK
Content-Length: 978
Content-Type: text/html; charset=utf-8
Date: Sat, 29 May 2021 18:18:17 GMT
Server: nginx/1.17.10
```

Multiple levels of sticky sessions may also be configured through the same
mechanism.
This is useful when there's a load balancer behind a load balancer.
We deemed this was outside the scope of this lab and are therefore not explained
in this document.
