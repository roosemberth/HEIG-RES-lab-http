import uuid

from flask import Flask, jsonify

app = Flask(__name__)

@app.route("/api/")
def index():
  return "<p>Hello, World! <a href='json'>here</a> is the json</p>"

@app.route("/api/json")
def json():
  data = {"random-uuid": uuid.uuid4()};
  return jsonify(data)

# Used only for development
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
